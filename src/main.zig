const std = @import("std");
const libgc = @import("gc");
const rl = @import("raylib");
const rlm = @import("raylib-math");
const math = std.math;
const List = std.ArrayList;
const assert = std.debug.assert;
const eq = std.meta.eql;
const sort = std.mem.sort;
const Color = rl.Color;
const Point = rl.Vector2;
const Edge = [2]Point;
const v2_ = Point.init;
const v2 = struct {
    const zero = Point.init(0, 0);
    const add = rlm.vector2Add;
    const dist = rlm.vector2Distance;
    const len = rlm.vector2Length;
    const normalize = rlm.vector2Normalize;
    const scale = rlm.vector2Scale;
    const sub = rlm.vector2Subtract;
    const rotate = rlm.vector2Rotate;
    fn angle(v: Point) f32 {
        return math.atan2(f32, v.y, v.x);
    }
};
/// exists to make gc use more visible
fn gc_allocator() std.mem.Allocator {
    return libgc.ALLOCATOR;
}
/// get one side of a polygon
/// `i` is within 0..polygon_points.len
pub fn polygon_edge(polygon_points: []const Point, i: usize) Edge {
    const ps = polygon_points;
    return Edge{ ps[i], ps[(i + 1) % ps.len] };
}
pub fn edge_len(e: Edge) f32 {
    return v2.dist(e[0], e[1]);
}

// magical numbers
/// cursor size
const cursor_size = 5.0;
const cursor_hotspot_radius = 8.0;
const evac_hotspot_radius = 16.0;
const tile_size = 32.0;
/// how much a wire vertex is away from the polygon vertex
/// too small -> sticky
/// too big -> might slip loose
/// floating point. *sighs*
const wire_route_clearance = 1e-3;

const debug = struct {
    const on = @import("builtin").mode == .Debug;
    var intersections: []const Intersection = &.{};
};

// hack: now used as patrol path with extra data
pub const Patrol = struct {
    polygon: Polygon,
    time_delay: f32 = 0.0,
    /// how much time does it look in front of it?
    time_look_ahead: f32 = tile_size * 3.0,
    speed: f32 = 1.0,

    pub fn whereAt(this: @This(), time: f32) Point {
        return this.polygon.whereAt((time + this.time_delay) * this.speed);
    }
};

pub const Polygon = struct {
    points: []const Point,
    center: Point,
    perimeter_length: f32,

    pub fn init(points: []const Point, center: Point) !@This() {
        const gc = gc_allocator();
        var total: f32 = 0.0;
        for (0..points.len) |i| {
            const e = polygon_edge(points, i);
            total += edge_len(e);
        }
        return .{
            .points = try gc.dupe(Point, points),
            .center = center,
            .perimeter_length = total,
        };
    }

    pub fn edge(this: @This(), i: usize) Edge {
        return polygon_edge(this.points, i);
    }

    pub fn drawLine(this: @This(), color: Color) void {
        const ps = this.points;
        rl.drawLineStrip(ps, color);
        rl.drawLineEx(ps[ps.len - 1], ps[0], 1, color);
    }

    pub fn whereAt(this: @This(), time: f32) Point {
        var remainder = @mod(time, this.perimeter_length);
        for (0..this.points.len) |i| {
            const e = this.edge(i);
            const d = edge_len(e);
            if (d >= remainder) {
                const ratio = remainder / d;
                return v2.add(v2.scale(e[0], (1.0 - ratio)), v2.scale(e[1], ratio));
            }
            remainder -= d;
        } else {}
        unreachable;
    }
};

/// find which point in closer to `reference`
fn Point_lessThan_distance(reference: Point, lhs: Point, rhs: Point) bool {
    return v2.dist(lhs, reference) < v2.dist(rhs, reference);
}
const Intersection = struct {
    vertex: Point,
    /// Choice of polygon_vertex on the edge
    polygon_vertex: Point,
    polygon_center: Point,
    // polygon_edge: Edge,
    fn lessThan_distance(reference: Point, lhs: @This(), rhs: @This()) bool {
        return Point_lessThan_distance(reference, lhs.vertex, rhs.vertex);
    }
};
fn calc_intersections(polygons: []const Polygon, line_segment: Edge, len_limit: ?usize) ![]Intersection {
    const gc = gc_allocator();
    var intersections_list = List(Intersection).init(gc);
    _: for (polygons) |poly| {
        for (0..poly.points.len) |i| {
            const edge = polygon_edge(poly.points, i);
            var vertex: Point = undefined;
            if (!eq(line_segment[0], vertex) and
                !eq(line_segment[1], vertex) and
                rl.checkCollisionLines(line_segment[0], line_segment[1], edge[0], edge[1], &vertex))
            {
                const direction = v2.sub(line_segment[1], line_segment[0]);
                const edge_direction = v2.sub(edge[1], edge[0]);
                const result = v2.rotate(direction, -v2.angle(edge_direction));
                const polygon_vertex = if (result.x >= 0) edge[1] else edge[0];

                try intersections_list.append(Intersection{ .vertex = vertex, .polygon_center = poly.center, .polygon_vertex = polygon_vertex });
                if (intersections_list.items.len == len_limit) break :_;
            }
        }
    }
    return intersections_list.toOwnedSlice();
}
/// move a line segment
/// result[_].polygon_vertex should be on one side of line_segment_prev but on the other side of line_segment
fn calc_intersections2(polygons: []const Polygon, line_segment_prev: Edge, line_segment: Edge, len_limit: ?usize) ![]Intersection {
    const gc = gc_allocator();
    var intersections_list = List(Intersection).init(gc);
    _: for (polygons) |poly| {
        for (0..poly.points.len) |i| {
            const edge = polygon_edge(poly.points, i);
            var vertex: Point = undefined;
            if (!eq(line_segment[0], vertex) and
                !eq(line_segment[1], vertex) and
                rl.checkCollisionLines(line_segment[0], line_segment[1], edge[0], edge[1], &vertex) and
                !rl.checkCollisionLines(line_segment_prev[0], line_segment_prev[1], edge[0], edge[1], &vertex))
            {
                const direction = v2.sub(line_segment[1], line_segment[0]);
                const edge_direction = v2.sub(edge[1], edge[0]);
                const result = v2.rotate(direction, -v2.angle(edge_direction));
                const polygon_vertex = if (result.x >= 0) edge[1] else edge[0];

                try intersections_list.append(Intersection{ .vertex = vertex, .polygon_center = poly.center, .polygon_vertex = polygon_vertex });
                if (intersections_list.items.len == len_limit) break :_;
            }
        }
    }
    return intersections_list.toOwnedSlice();
}

fn inside_polygons(polygons: []const Polygon, p: Point) ?usize {
    for (polygons, 0..) |poly, i| {
        inline for (0..8) |dir| {
            const angle = math.pi / 4.0 * @as(f32, @floatFromInt(dir));
            const p_ex = v2.add(p, v2_(math.cos(angle), math.sin(angle)));
            if (rl.checkCollisionPointPoly(p_ex, poly.points)) return i;
        }
    }
    return null;
}

const string = []const u8;

// Tiled level and data structures
pub const Level = struct {
    /// point of interest
    const Tiled_Poi = struct {
        id: u32,
        x: f32,
        y: f32,
        name: string,
    };
    const Tiled_Rectangle = struct {
        id: u32,
        x: f32,
        y: f32,
        width: f32,
        height: f32,

        pub fn get_polygon(o: @This()) !Polygon {
            const x = o.x;
            const y = o.y;
            const w = o.width;
            const h = o.height;
            return try Polygon.init(
                &.{ v2_(x, y), v2_(x, y + h), v2_(x + w, y + h), v2_(x + w, y) },
                v2_(x + w / 2, y + h / 2),
            );
        }
    };
    const Tiled_Polygon = struct {
        id: u32,
        x: f32,
        y: f32,
        polygon: []const Point,

        pub fn get_polygon(this: @This()) !Polygon {
            const gc = gc_allocator();
            const ps = try gc.dupe(Point, this.polygon);
            const origin = v2_(this.x, this.y);
            for (ps) |*p| {
                p.* = v2.add(p.*, origin);
            }

            // hack: stupid center algorithm
            var overall = v2.zero;
            for (ps) |p| {
                overall = v2.add(overall, p);
            }
            const center = v2.scale(overall, 1.0 / @as(f32, @floatFromInt(ps.len)));

            return try Polygon.init(ps, center);
        }
    };
    width: f32,
    height: f32,
    specials: []const Tiled_Poi,
    obstacles: []const Polygon,
    patrols: []const Patrol,

    fn parse(comptime T: type, o: std.json.ObjectMap) !T {
        const gc = gc_allocator();
        const parsed = try std.json.parseFromValueLeaky(T, gc, std.json.Value{ .object = o }, .{ .ignore_unknown_fields = true, .allocate = .alloc_always });
        return parsed;
    }
    fn get_property(o: std.json.ObjectMap, comptime PropType: type, property_name: []const u8) ?PropType {
        const properties = (o.get("properties") orelse return null).array.items;
        for (properties) |_prop| {
            const prop = _prop.object;
            if (std.mem.eql(u8, prop.get("name").?.string, property_name)) {
                return std.json.parseFromValueLeaky(PropType, std.testing.failing_allocator, prop.get("value").?, .{}) catch unreachable;
            }
        }
        return null;
    }

    pub fn loadFromFile(filename: []const u8) !@This() {
        const gc = gc_allocator();
        const content = try std.fs.cwd().readFileAlloc(gc, filename, 1 << 20); // limit to 1MB
        defer gc.free(content);
        const _root = try std.json.parseFromSlice(std.json.Value, gc, content, .{});
        defer _root.deinit();
        const root = _root.value.object;
        const width = root.get("width").?.integer * root.get("tilewidth").?.integer;
        const height = root.get("height").?.integer * root.get("tileheight").?.integer;
        const layers = root.get("layers").?.array.items;
        var specials = List(Tiled_Poi).init(gc);
        var obstacles = List(Polygon).init(gc);
        var patrols = List(Patrol).init(gc);

        for (layers) |_layer| {
            const layer = _layer.object;
            const name = if (layer.get("name")) |x| x.string else "";
            const objects = layer.get("objects").?.array.items;
            for (objects) |_o| {
                const o = _o.object;
                if (o.get("visible").?.bool) {
                    if (o.contains("point")) {
                        try specials.append(try parse(Tiled_Poi, o));
                    } else if (std.mem.eql(u8, name, "obstacles")) {
                        const poly = _: {
                            if (o.contains("polygon")) {
                                break :_ try (try parse(Tiled_Polygon, o)).get_polygon();
                            } else {
                                break :_ try (try parse(Tiled_Rectangle, o)).get_polygon();
                            }
                        };
                        try obstacles.append(poly);
                    } else if (std.mem.eql(u8, name, "patrols")) {
                        var parsed = try parse(Tiled_Polygon, o);
                        var d = Patrol{
                            .polygon = try parsed.get_polygon(),
                        };
                        if (get_property(o, f32, "speed")) |x| d.speed = x;
                        try patrols.append(d);
                    } else {
                        // ignoring extranuous objects
                        const stderr = std.io.getStdErr().writer();
                        try stderr.writeAll("warning: ignoring object.  ");
                        try std.json.stringify(_o, .{}, stderr);
                    }
                }
            }
        }
        return @This(){
            .width = @floatFromInt(width),
            .height = @floatFromInt(height),
            .specials = try specials.toOwnedSlice(),
            .obstacles = try obstacles.toOwnedSlice(),
            .patrols = try patrols.toOwnedSlice(),
        };
    }
};

pub const GameStage = struct {
    width: f32,
    height: f32,

    // static obstacles
    obstacles: []const Polygon,
    patrols: []const Patrol,
    specials: []const Level.Tiled_Poi,
    evacs: []const Point,

    // mutable state:

    /// turing points of the wire
    wire_list: List(Point),
    moving_wire: bool = false,

    const TemporaryState = struct {
        time: f32,
        patrols_pos: []const Point,
        patrols_lookat: []const Point,
    };

    fn wire_mut(this: *@This()) *[]Point {
        return &this.wire_list.items;
    }
    fn wire_ref(this: @This()) []Point {
        return this.wire_list.items;
    }
    fn start_pos(this: @This()) Point {
        return this.wire_mut().*[0];
    }
    fn end_pos(this: @This()) Point {
        return this.wire_ref()[this.wire_ref().len - 1];
    }

    fn update(this: *@This()) !TemporaryState {
        var first_frame_move = false; // todo: this is hella janky. the update function should be run only on input events.
        const xy = rl.getMousePosition();
        if (rl.isMouseButtonPressed(.mouse_button_left)) {
            if (this.moving_wire) {
                this.moving_wire = false;
            } else if (v2.dist(xy, this.end_pos()) <= cursor_hotspot_radius) {
                this.moving_wire = true;
                first_frame_move = true;
            }
        }
        if (this.moving_wire)
            try this.update_wire(first_frame_move);
        const time = _: {
            const xs = this.wire_ref();
            var total: f32 = 0;
            for (xs[0 .. xs.len - 1], xs[1..]) |p, q| {
                total += v2.dist(p, q);
            }
            break :_ total;
        };
        const ps_ptl = try this.update_patrol(time);
        return TemporaryState{
            .time = time,
            .patrols_pos = ps_ptl.pos,
            .patrols_lookat = ps_ptl.lookat,
        };
    }

    fn update_patrol(this: *@This(), time: f32) !struct { pos: []const Point, lookat: []const Point } {
        const gc = gc_allocator();
        const ps = try gc.alloc(Point, this.patrols.len);
        const ps2 = try gc.alloc(Point, this.patrols.len);
        for (this.patrols, ps, ps2) |ptl, *p, *p2| {
            p.* = ptl.whereAt(time);
            p2.* = ptl.whereAt(time + ptl.time_look_ahead);
        }
        return .{ .pos = ps, .lookat = ps2 };
    }

    /// both convex polygons
    fn poormans_checkCollisionPolyPoly(bigger: Polygon, smaller: []const Point) bool {
        _ = bigger;
        _ = smaller;

        // todo
        return false;
    }

    fn update_wire(this: *@This(), first_frame_move: bool) !void {
        const xs = this.wire_mut();
        assert(xs.len >= 2);
        const xy = rl.getMousePosition();
        const xy_diff = rl.getMouseDelta();
        const mouse_moved = first_frame_move or !eq(xy_diff, v2_(0, 0));
        const mouse_outside_polygon = inside_polygons(this.obstacles, xy) == null;
        if (mouse_moved and mouse_outside_polygon) {
            const xy_prev = xs.*[xs.len - 1];
            xs.*[xs.len - 1] = xy;

            if (xs.len >= 3) { // remove redundante wire points
                var i: usize = 0;
                while (i + 2 < xs.len) { // trigger when at least 3 points
                    const segment = Edge{ xs.*[i], xs.*[i + 2] };
                    const intersections2 = try calc_intersections(this.obstacles, segment, 1);
                    var ok_to_remove = intersections2.len == 0;

                    if (ok_to_remove)
                        for (this.obstacles) |poly| {
                            if (poormans_checkCollisionPolyPoly(poly, xs.*[i .. i + 3])) {
                                ok_to_remove = false;
                                break;
                            }
                        };

                    if (ok_to_remove)
                        _ = this.wire_list.orderedRemove(i + 1)
                    else
                        i += 1;
                }
            }

            var _point_added = false;
            const last_segment = Edge{ xs.*[xs.len - 2], xs.*[xs.len - 1] };
            const last_segment_prev = Edge{ xs.*[xs.len - 2], xy_prev };
            const intersections = try calc_intersections2(this.obstacles, last_segment_prev, last_segment, null);
            debug.intersections = intersections;
            // sort. earliest in time first.
            sort(Intersection, intersections, last_segment[0], Intersection.lessThan_distance);
            for (intersections) |earliest| {
                const center = earliest.polygon_center;
                const winner = earliest.polygon_vertex;
                const new_point = v2.add(winner, v2.scale(v2.normalize(v2.sub(winner, center)), wire_route_clearance));
                const is0 = try calc_intersections(this.obstacles, .{ last_segment[0], new_point }, 1);
                const is1 = try calc_intersections(this.obstacles, .{ new_point, last_segment[1] }, 1);
                if (is0.len == 0 and is1.len == 0) {
                    try this.wire_list.insert(xs.len - 1, new_point);
                    _point_added = true;
                    break;
                }
            }
        }
    }

    fn draw(this: @This(), situation: TemporaryState) !void {
        const gc = gc_allocator();

        const xy = rl.getMousePosition();
        const xs = this.wire_ref();
        for (this.evacs) |p| {
            rl.drawCircleV(p, evac_hotspot_radius, Color.gray);
        }
        for (this.obstacles) |poly| {
            poly.drawLine(Color.ray_white);
        }
        for (this.patrols, situation.patrols_pos, situation.patrols_lookat) |patrol, pos, lookat| {
            patrol.polygon.drawLine(Color.sky_blue);
            rl.drawCircleLinesV(pos, cursor_hotspot_radius, Color.sky_blue);
            rl.drawLineV(pos, lookat, Color.yellow);
        }
        rl.drawLineStrip(xs, Color.orange);
        rl.drawLineEx(v2_(xy.x, xy.y - cursor_size - 1), v2_(xy.x, xy.y + cursor_size), 1, Color.white);
        rl.drawLineEx(v2_(xy.x - cursor_size - 1, xy.y), v2_(xy.x + cursor_size, xy.y), 1, Color.white);

        rl.drawCircleLinesV(this.end_pos(), cursor_hotspot_radius, Color.orange);

        if (debug.on) {
            if (debug.intersections.len > 0) {
                rl.drawCircleV(debug.intersections[0].vertex, 2.5, Color.white);
                rl.drawCircleV(debug.intersections[0].polygon_vertex, 2.5, Color.white);
                for (debug.intersections[1..]) |o| {
                    rl.drawCircleV(o.vertex, 2.5, Color.lime);
                }
            }
        }

        const s = try std.fmt.allocPrintZ(gc, "t={d:.2}", .{situation.time});
        rl.drawText(s, 0, 0, 10, Color.white);
    }
};
pub fn load_map(map_data: Level) !GameStage {
    const gc = gc_allocator();

    const obstacles = map_data.obstacles;

    const patrols = map_data.patrols;

    var evac_list = List(Point).init(gc);
    var maybe_start: ?Level.Tiled_Poi = null;
    for (map_data.specials) |o| {
        if (std.mem.eql(u8, o.name, "start")) {
            maybe_start = o;
        }
        if (std.mem.startsWith(u8, o.name, "evac")) {
            try evac_list.append(Point.init(o.x, o.y));
        }
    }
    const start = if (maybe_start) |o| v2_(o.x, o.y) else return error.Game_MapNoStart;

    var wire_list = List(Point).init(gc);
    try wire_list.appendSlice(&.{
        start,
        start,
    });

    return GameStage{
        .width = map_data.width,
        .height = map_data.height,
        .obstacles = obstacles,
        .patrols = patrols,
        .specials = map_data.specials,
        .evacs = try evac_list.toOwnedSlice(),
        .wire_list = wire_list,
    };
}

pub fn main() !void {
    libgc.init();
    libgc.setAllInteriorPointers(true);

    const argv = std.os.argv;
    const map_filename = if (argv.len > 1) std.mem.span(argv[1]) else "levels/level_test.tmj";
    var stage = try load_map(try Level.loadFromFile(map_filename));
    rl.initWindow(@intFromFloat(stage.width), @intFromFloat(stage.height), "hello");
    rl.setWindowState(.flag_vsync_hint);
    rl.hideCursor();
    while (!rl.windowShouldClose()) {
        const temp = try stage.update();
        rl.beginDrawing();
        rl.clearBackground(Color.black);
        try stage.draw(temp);
        rl.endDrawing();
    }
}
