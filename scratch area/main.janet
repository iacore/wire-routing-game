# original wire routing implementation

(defn e2-distance [[x0 y0] [x1 y1]] (math/sqrt (+ (math/pow (- x1 x0) 2) (math/pow (- y0 y1) 2))))
(defn v2/+ [[x0 y0] [x1 y1]] [(+ x0 x1) (+ y0 y1)])
(defn v2/- [[x0 y0] [x1 y1]] [(- x0 x1) (- y0 y1)])
(defn v2/normalize [[x y]]
  (def l (e2-distance [0 0] [x y]))
  [(/ x l) (/ y l)])

(def ss 50)

(def polygons @[])

(each xx (range 50 400 100)
  (each yy (range 50 400 100)
    (array/push polygons
     {:points [[xx yy] [xx (+ yy ss)] [(+ xx ss) (+ yy ss)] [(+ xx ss) yy]]
      :center [(+ xx (/ ss 2)) (+ yy (/ ss 2))]})))

(def polygons (freeze polygons))

(def wire @[[225 450] [255 450]])


(use jaylib)
(init-window 450 450 "Test Game")
(set-target-fps 60)
(hide-cursor)



(while (not (window-should-close))
  (begin-drawing)

  (let [xy (get-mouse-position)]
    (if (not= xy [0 0])
      (set (wire (- (length wire) 1)) xy)))
  (def last-segment (slice wire (- (length wire) 2)))

  (clear-background [0 0 0])
  (draw-line-strip
    wire
    :orange)
  (def intersections @[]) # [[vertex polygon-edge polygon-center] ...]
  (each {:points polygon :center polygon-center} polygons
    (draw-line-strip
      [(splice polygon) (polygon 0)]
      :ray-white)
    (each i (range (length polygon))
      (def edge [(polygon i) (polygon (mod (+ i 1) (length polygon)))])
      (def vertex
        (check-collision-lines
         (splice last-segment)
         (splice edge)))
      (when vertex
        (array/push intersections [vertex edge polygon-center])
        (draw-line-strip
          edge
          :sky-blue)
        (pp vertex)
        (draw-circle-gradient (splice (map math/round vertex)) 4.0 :yellow :lime))))

  (sort-by (fn [[vertex _]] (e2-distance vertex (last-segment 0))) intersections)

  (when (not (empty? intersections))
    (def center (get-in intersections [0 2]))
    (def candidates @[
                       (get-in intersections [0 1 0])
                       (get-in intersections [0 1 1])])
    (sort-by |(e2-distance $ (last-segment 0)) candidates)
    (def winner (candidates 1))
    #(draw-circle (splice (map math/round winner)) 4.0 :white)

    (array/insert wire -2 (v2/+ winner (v2/normalize (v2/- winner center)))))

  (end-drawing))

(close-window)
