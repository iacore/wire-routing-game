#!/usr/bin/env janet
# Convert Tiled Map to ZON

(import spork/json)

(defn zon/encode [o]
  (defn gen-array []
    (string "&.{"
            (splice (interpose "," (map zon/encode o)))
            "}"))
  (defn gen-lit []
    (string/format "%q" o))
  (defn gen-table []
    (defn encode-pair [[k v]]
      (string "." k "=" (zon/encode v)))
    (string ".{"
            (splice (interpose "," (map encode-pair (pairs o))))
            "}"))
  (match (type o)
    :boolean (gen-lit)
    :string  (gen-lit)
    :number  (gen-lit)
    :tuple   (gen-array)
    :array   (gen-array)
    :struct  (gen-table)
    :table   (gen-table)
    rest     (assert false (string/format "invalid object type: %q" rest))))

(defn filter-kvs-helper [o keys]
  (->> o
       (pairs)
       (filter |(index-of ($ 0) keys))
       (from-pairs)))

(defmacro filter-kvs [name keys]
  ~(def ,name (,map |(,filter-kvs-helper $ ,keys) ,name)))

(defn tiled-to-zon [input]
  (def mapdata (json/decode input))
  (def specials @[])
  (def obstacles @[])
  (def patrols @[])

  (defn filter-objects [layer]
    (def name (layer "name"))
    (each o (layer "objects")
      (if (o "visible")
        (cond
          (o "point")
          (array/push specials o)
          (= name "obstacles")
          (array/push obstacles o)
          (= name "patrols")
          (array/push patrols o)))))

  (def layers (map filter-objects (mapdata "layers")))
  
  (filter-kvs specials ["id" "name" "x" "y"])
  (filter-kvs obstacles ["id" "x" "y" "width" "height"])
  (filter-kvs patrols ["id" "x" "y" "polygon"])
  
  (zon/encode
   {:width (* (mapdata "width") (mapdata "tilewidth"))
    :height (* (mapdata "height") (mapdata "tileheight"))
    :specials specials
    :obstacles obstacles
    :patrols patrols}))

(defn- main [_ infile]
  (assert (string/has-suffix? ".tmj" infile))
  (->> infile
       (slurp)
       (tiled-to-zon)
       (print)))
