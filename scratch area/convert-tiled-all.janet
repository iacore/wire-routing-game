#!/usr/bin/env janet
# Convert All Tiled Maps to a Zig file

(use ./convert-tiled)

(defn- main [_ dir]
  (print `const Level = @import("root").Level;`)
  (each file (os/dir dir)
    (when (string/has-suffix? ".tmj" file)
      (def name (string/slice file 0 -5))
      (def path (string dir "/" file))
      (prin "pub const " name ": Level = ")
      (prin (tiled-to-zon (slurp path)))
      (prin ";\n"))))
