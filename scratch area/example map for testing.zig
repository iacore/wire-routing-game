pub fn load_example_map_old() !GameStage {
    const gc = gc_allocator();

    const obstacles = _: {
        var polygons_list = List(Polygon).init(gc);
        const s = 50.0; // square edge len
        var x: f32 = 40;
        while (x < 400) : (x += 100) {
            var y: f32 = 40;
            while (y < 400) : (y += 100) {
                try polygons_list.append(
                    try Polygon.init(
                        &.{ v2_(x, y), v2_(x, y + s), v2_(x + s, y + s), v2_(x + s, y) },
                        v2_(x + s / 2, y + s / 2),
                    ),
                );
            }
        }
        break :_ try polygons_list.toOwnedSlice();
    };

    const patrols = _: {
        var polygons_list = List(Polygon).init(gc);
        const s = 90.0; // square edge len
        var x: f32 = 20;
        var add = false;
        while (x < 400) : ({
            x += 100;
            add = !add;
        }) {
            var y: f32 = 20;
            while (y < 400) : ({
                y += 100;
                add = !add;
            }) {
                if (add)
                    try polygons_list.append(
                        try Polygon.init(
                            &.{ v2_(x, y), v2_(x, y + s), v2_(x + s, y + s), v2_(x + s, y) },
                            v2_(x + s / 2, y + s / 2),
                        ),
                    );
            }
        }
        break :_ try polygons_list.toOwnedSlice();
    };

    var wire_list = List(Point).init(gc);
    try wire_list.appendSlice(&.{
        v2_(225, 450),
        v2_(225, 450),
    });

    return GameStage{
        .obstacles = obstacles,
        .patrols = patrols,
        .wire_list = wire_list,
    };
}
