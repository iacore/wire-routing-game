the algorithm is still buggy as hell. need box2d to save the day

## build and run

This program is buildable with Zig 0.11.0.

```shell
git submodule update --init --recursive --depth 1
zig build run
zig build run -- custom-map.tmj
```

## controls

Click on the orange circle and move cursor around. Click again to drop.

## map making

Maps are made using [Tiled](https://www.mapeditor.org/) and saved as `.tmj` (Tiled JSON).

Currently, there are no fixed game rules. You make them up yourself.

Some Example Rules that goes with the default map [levels/level_test.tmj](levels/level_test.tmj)
PC being looked at by patrol -> level fail.
Objectives can be imagined as small squares. Player must touch/tangle all of them before reaching the exit.
Time took (shown as `t=<time in pixels>` at top-left) is score (lower=better).

map shape rotation not supported.

## to do

- when the game is ready, get rid of gc for a wasm build
 
- evac points
- stomp enemy w/ newton's method. hover over enemy long enough to stomp.
- enermy view cone w/ fan-out ray casting. blind spot near itself (view cone does not start from the body).
- temporal axis (isometric 3D with time axis added)

- z-levels (floors) connected using stairs


## story

story can come later
