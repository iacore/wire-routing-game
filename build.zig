const std = @import("std");

fn depend(step: *std.Build.Step, path: []const u8) void {
    std.Build.LazyPath.relative(path).addStepDependencies(step);
}
fn depend_1deep(b: *std.Build, step: *std.Build.Step, path: []const u8) !void {
    const dir_levels = try std.fs.cwd().openDir(path, .{ .iterate = true });
    var w = try dir_levels.walk(b.allocator);
    while (try w.next()) |entry| {
        depend(step, entry.path);
    }
}

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const dep_rl = b.dependency("raylib", .{ .target = target, .optimize = optimize });
    const dep_gc = b.anonymousDependency("lib_gc", @import("lib_gc/build.zig"), .{ .target = target, .optimize = optimize });

    const exe = b.addExecutable(.{
        .name = "wire-routing",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    exe.addModule("raylib", dep_rl.module("raylib"));
    exe.addModule("raylib-math", dep_rl.module("raylib-math"));
    exe.linkLibrary(dep_rl.artifact("raylib"));
    exe.addModule("gc", dep_gc.module("gc"));
    exe.linkLibrary(dep_gc.artifact("gc"));

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const exe_unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    const run_exe_unit_tests = b.addRunArtifact(exe_unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_exe_unit_tests.step);
}
